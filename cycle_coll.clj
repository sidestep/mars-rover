(ns cycle-coll)

(defn make[coll idx]
 ;Makes a coll cyclically traversable in both directions.
 ;idx - set current index. 
  (let [vcoll (vec coll)
        max-idx (dec (count vcoll))]
    (assert (<= 0 idx max-idx))
    {
     :idx idx 
     :coll vcoll 
     :max-idx max-idx
     }))

(defn next[{:keys [idx max-idx] :as ccoll}]
   (assoc ccoll :idx (if (>= idx max-idx) 
                       0 
                       (inc idx))))

(defn prev[{:keys [idx max-idx] :as ccoll}]
   (assoc ccoll :idx (if (<= idx 0)
                       max-idx 
                       (dec idx))))

(defn curr[{:keys [idx coll]}]
;Returns a value under current position.
  (coll idx))

; tests and usage examples 
(comment 
  (let [cc1 (make [0] 0)
        cc2 (make [1 2 3] 0)]
  (assert (= (curr cc1)
              0))
  (assert (= (curr (prev cc1))
              0))
  (assert (= (curr (prev (prev cc1)))
              0))
  (assert (= (curr cc2)
             1))
  (assert (= (curr (next cc2))
             2))
  (assert (= (curr (prev cc2))
             3))
  (assert (= (curr (prev (prev cc2)))
             2))
  (assert (= (curr (next (prev cc2)))
             1))))
