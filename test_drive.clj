(ns test-drive
  (:use [rover]))

(let [rover (land-rover [0 0] N [2 2])]
  (assert (= (location rover)
             [0 0 N]))
  (assert (= (location (navigate rover [F]))
             [0 1 N]))
  (assert (= (location (navigate rover [R F]))
             [1 0 E]))
  (assert (= (try 
               (navigate rover [L F])
               (catch Exception e (:reason (ex-data e))))
             :rover/out-of-grid))
  (assert (= (try 
               (navigate rover [L L F])
               (catch Exception e (:reason (ex-data e))))
             :rover/out-of-grid ))
  (assert (= (location (navigate rover [L L L F]))
             [1 0 E])))

(let [rover (land-rover [0 0] S [10 10])]
  (assert (= (location rover)
             [0 0 S]))
  (assert (= :rover/out-of-grid 
             (try 
               (navigate rover [F])
               (catch Exception e (:reason (ex-data e))))))
  (assert (= :rover/out-of-grid 
             (try 
               (navigate rover [R F])
               (catch Exception e (:reason (ex-data e))))))
  (assert (= :rover/out-of-grid 
             (try 
               (navigate rover [L R F])
               (catch Exception e (:reason (ex-data e))))))
  (assert (= (location (navigate rover [L L F]))
             [0 1 N]))
  (assert (= (location (navigate rover [L L F R F]))
             [1 1 E]))
  (assert (= (location (navigate rover [L F L F F F R]))
             [1 3 E]))
  (assert (= (location (navigate rover [L F L F F F]))
             [1 3 N]))
  (assert (= (location (navigate rover [L F R R F L]))
             [0 0 S])))
