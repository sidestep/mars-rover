(ns rover
  (:require [cycle-coll]))

(defn within-bounds?[[x y] [max-x max-y]]
  (and 
    (< -1 x max-x)
    (< -1 y max-y)))

;--- moves
(defn N [[x y]] [x (inc y)]) 
(defn E [[x y]] [(inc x) y]) 
(defn S [[x y]] [x (dec y)]) 
(defn W [[x y]] [(dec x) y])
;--- end moves

;--- maneuvers
(defn L[{m :move :as rover}]
  (assoc rover :move (cycle-coll/prev m)))
   
(defn R[{m :move :as rover}]
  (assoc rover :move (cycle-coll/next m)))

(defn F[{m :move p :pos g :grid :as rover}]
  (let [new-pos ((cycle-coll/curr m) p)]
    (if-not (within-bounds? new-pos g) 
      (throw (ex-info "Can't navigate out of grid" 
                      {:reason ::out-of-grid})))
  (assoc rover :pos new-pos)))
;--- end maneuvers

(defn land-rover[xy dir grid]
  "Initializes a rover"
  (let [moves [N E S W]
        move-idx (.indexOf moves dir)]
    (assert (not= -1 move-idx))
    (assert (within-bounds? xy grid))
    {
     :grid grid 
     :move (cycle-coll/make moves move-idx) 
     :pos xy 
    }))

(defn location[{[x y] :pos m :move}]
  "Returns location of a rover"
  [x y (cycle-coll/curr m)])

(defn move[rover m]
  (m rover))

(defn navigate[rover maneuvers]
  (reduce move rover maneuvers))

(comment ;test/example 
  (-> (land-rover [0 0] E [5 10])
      (navigate [F L F R F])
      (location)
      (= [2 1 E])))
